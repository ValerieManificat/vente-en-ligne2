<!-- <nav class="navbar navbar-expand-lg bg-body-tertiary"> -->
<!-- <nav class="navbar navbar-expand-lg bg-dark"> -->
<!-- <nav class="navbar navbar-expand-lg" style="background-color: black;"> -->
<nav class="navbar navbar-expand-lg navbar-dark" style="background-color: black;">
  <div class="container-fluid">
  <a class="navbar-brand" href="#"><img src="fashion-store.png" width="200" height="200" alt="Logo"></a>
    <a class="navbar-brand" href="#">Liste des articles</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <!-- <div class="collapse navbar-collapse bg-white" id="navbarText"> -->
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="#">Accueil</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Se connecter</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Se déconnecter</a>
        </li>
      </ul>
      <span class="navbar-text">
      "Explorez le style éco-responsable avec notre sélection <br>soigneusement choisie de vêtements d'occasion."
      </span>
    </div>
  </div>
</nav>