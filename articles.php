<!-- établir la connexion: -->
<?php require_once("connexion.php");
$ps = $pdo->prepare("select * from articles;");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <!-- Option 1: Include in HTML -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
    <title>Liste des articles</title>
</head>
<body>
<style>
    tr:hover {background-color: #D6EEEE;}
body, html {
  height: 100%;
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.hero-image {
  background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url("vetementoccasion.png");
  height: 50%;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  position: relative;
}

.hero-text {
  text-align: center;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  color: white;
}

.hero-text button {
  border: none;
  outline: 0;
  display: inline-block;
  padding: 10px 25px;
  color: black;
  background-color: #ddd;
  text-align: center;
  cursor: pointer;
}

.hero-text button:hover {
  background-color: #555;
  color: white;
}
</style>
</head>
<body>

<div class="hero-image">
  <div class="hero-text">
    <h1 style="font-size:50px">Bienvenue sur mon site de vente de vêtements d'occasion</h1>
   
    <button>Qui sommes nous?</button>
  </div>
</div>


 <!--entete - barre de navigation  -->
<?php
require_once("entete.php");
?>
<!-- contenu de notre page => tableau html -->
<div class="container" style="margin-top:60px;">
        <div class="card">
            <div class="card-header">
                <h3 class="text-center">Liste des articles</h3>
            </div>
            <div class="card-body"></div>
            <table class="table table-striped">
                <tr>
                   
                    <td>Titre de l'article</td>
                    <td>Decription de l'article</td>
                    <td>Date de publicaton de l'article</td>
                    <td>Photo</td>
                </tr>
                <?php while ($articles = $ps->fetch()) { ?>
                    <tr>
                        <td><?php echo $articles["titreArticle"]; ?></td>
                        <td><?php echo $articles["descriptionArticle"]; ?></td>
                        <td><?php echo $articles["datePublicationArticle"]; ?></td>
                        
                        <td><img src='<?php echo $articles["photo"]; ?>' width="100" height="100"></td>
                        
                       
                            <td></td>
                            <td></td>
                        <?php } ?>
                    </tr>
               
            </table>
        </div>
    </div>

</body>
</html>


