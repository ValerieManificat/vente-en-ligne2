<?php // requete de la page pour la connexion
require_once 'inc/init.inc.php';

// 2- Je fais ma requete pour sélectionner les 15 annonces les plus recentes de ma BDD
$requete = $pdoBiomimetisme->query("SELECT * FROM articles ORDER BY id_articles DESC LIMIT 0,15")
?>
<!doctype html>
<html lang="fr">

<head>
    <title>Le biomimétisme</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS v5.2.1 -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">

</head>

<body>
    <?php require_once 'inc/nav.inc.php'; ?>
     <!-- DEBUT HEADER-->
     <header class="p-5 mb-4 bg-light rounded-3" style="background-image: url('img/feuille-verte-400-250.jpg');">
        <section class="container-fluid py-5" style="background-image: url('img/feuille-verte-400-250.jpg');">
            <div class="mt-4 p-5 rounded text-white" style="background-image: url('img/feuille-verte-400-250.jpg');">
                <h1 class="display-5 fw-bold">Le Biomimétisme</h1>
                <p class="col-mb-8">Les 15 articles ajoutés récemment sur le site.</p>
            </div>
        </section>
    </header> <!-- FIN HEADER  -->

    <main class="container">
        <section class="row">
            <?php while ($articles = $requete->fetch(PDO::FETCH_ASSOC)) { // OUVERTURE DE LA BOUCLE WHILE // ce n'est pas parce que je ne suis plus dans passage PHP que ma boucle ne continue pas. Tant que mon acolade n'est pas fermée dans un passage PHP, je serai toujours à l'intérieur de ma boucle 
            ?>



                <div class="col-12 col-md-6 col-lg-3 mb-5">
                    <div class="card text-center">
                        <img class="card-img-top img-fluid" src="<?php echo $articles['photo']; ?>" alt="Illustration annonce">
                        <div class="card-body opacity-50">
                            <h2 class="card-titre fs-6"><?php echo $articles['titre'];  ?></h2>
                            <p class="card-text"><?php echo substr($articles['description'], 0, 30) ?>...<a href="article.php?id_articles=<?php echo $articles['id_articles'] ?>" class="btn btn-primary btn-sm mt-1">CONSULTER L'ARTICLE</a> </p>
                        </div>
                        <div class="card-footer">
                            <p class="text-capitalize"><?php echo $articles['titre']; ?></p>


                        </div>
                    </div>
                </div>

                <!-- ici j'affiche ma card spéciale (déjà réservé) -->
                <!-- <div class="col-12 col-md-6 col-lg-3 mb-5">
                    <div class="card text-center position-relative">
                        <img class="card-img-top img-fluid opacity-50" src="<?php echo $articles['photo']; ?>" alt="Illustration annonce">
                        <div class="card-body">
                            <h2 class="card-titre fs-6"><?php echo $articles['titre'];  ?></h2>
                            <p class="card-text"><?php echo substr($articles['description'], 0, 30) ?>...<a href="article.php?id_articles=<?php echo $articles['id_articles'] ?>" class="btn btn-primary btn-sm mt-1">CONSULTER L'ARTICLE</a> </p>
                        </div>
                        <div class="card-footer opacity-50">
                            <p class="text-capitalize"><?php echo $articles['categorie']; ?></p>

                            


                        </div>

                    </div>

                </div>
 -->















            <?php } ?>
        </section>
    </main>


    <!-- Bootstrap JavaScript Libraries -->
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous">
    </script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.min.js" integrity="sha384-7VPbUDkoPSGFnVtYi0QogXtr74QeVeeIs99Qfg5YCF+TidwNdjvaKZX19NZ/e6oz" crossorigin="anonymous">
    </script>
</body>

</html>